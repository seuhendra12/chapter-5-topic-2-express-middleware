const express = require("express");
const app = express();

const morgan = require("morgan");

// Ambil port dari environment variable
// Dengan nilai default 8000
const PORT = process.env.PORT || 8000;

// third party middleware
app.use(express.urlencoded())
app.use(morgan('combined'))

// middleware buatana sendiri
app.use((req, res, next) => {
  const dateNow = Date.now();
  const requestTime = new Date(dateNow).toDateString();
  console.log(requestTime)

  next();  
})

// Our own middleware
function jalanAja(ini, itu, lanjut) {
  console.log('jalan aja gih')
  lanjut();
}

// Pengecekan
function isAdmin(req, res, next) {
  if (req.query.iam === "admin") {
    next();
    return
  }

  res.status(401).send("Kamu bukan admin");
}

// Function
function getBooks(req, res, next) {
  console.log('masuk gak yah ?')
  console.log(req.query)
  res
    .status(200)
    .json({
      'test' : '123'
    });
}

// Log info waktu ngehit API
// function getRequestTime(req, res, next) {
//   const dateNow = Date.now();
//   const requestTime = new Date(dateNow).toDateString();
//   console.log(requestTime)

//   next();
// }

// GET /api/v1/books?author=
app.get("/api/v1/books", jalanAja, isAdmin, getBooks);

// POST /api/v1/books
app.post("/api/v1/books", isAdmin, (req, res) => {
  
  if (req.query.iam === "admin") {
    res.status(401).send("Kamu bukan admin");
  }

  console.log(req.body)
  res
    .status(201)
    .send("Terima kasih sudah menambahkan buku di dalam database kami");
});

// PUT /api/v1/books/:id
app.put("/api/v1/books/:id", isAdmin, (req, res) => {

  if (req.query.iam === "admin") {
    res.status(401).send("Kamu bukan admin");
  }

  console.log(req.body)
  res
    .status(200)
    .send("Sudah diupdate!"); 
});

app.listen(PORT, () => {
  console.log(`Express nyala di http://localhost:${PORT}`);
});
